package component.containers
{
	import spark.components.SkinnableContainer;
	
	public class GroupBox extends SkinnableContainer
	{
		[Bindable]
		public var label : String;
		
		public function GroupBox()
		{
			super();
		}
	}
}
